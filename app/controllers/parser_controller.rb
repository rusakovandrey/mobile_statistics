require 'csv'
# Main
class ParserController < ApplicationController
  include Parsers
  include Jsons
  include Utils

  def init
    GeneralShare.delete_all
    parser_csv(GeneralShare, "#{Rails.root}/files/general_share.csv")
    netmarketshare_parser_general_share
    statcounter_parser_general_share
    unity3d_parser_general_share
    w3schools_parser_general_share

    IosVersion.delete_all
    parser_csv(IosVersion, "#{Rails.root}/files/ios_version.csv")
    unity3d_parser_ios
    david_smith_parser_ios
    apple_parser_ios
    apteligent_parser_ios
    bugfender_parser_ios

    AndroidVersion.delete_all
    parser_csv(AndroidVersion, "#{Rails.root}/files/android_versions.csv")
    bugfender_parser_android_version
    android_parser_android_version
    appbrain_parser_android_version
    apteligent_parser_android_version
    unity3d_parser_android_version

    IosDevice.delete_all
    parser_csv(IosDevice, "#{Rails.root}/files/ios_devices.csv")
    bugfender_parser_ios_devices
    david_smith_parser_ios_devices
    unity3d_parser_ios_devices

    ScreenResolution.delete_all
    parser_csv(ScreenResolution, "#{Rails.root}/files/screen_resolutions.csv")
    statcounter_parser_screen_resolution
    # unity3d_parser_screen_resolution # disabled because of strange statistics

    AndroidDevice.delete_all
    parser_csv(AndroidDevice, "#{Rails.root}/files/android_devices.csv")
    appbrain_parser_android_devices
    redirect_to :back
  end

  def index
    @general_shares_json = general_shares_json
    @major_ios_versions_json = major_ios_versions_average_json
    @android_versions_json = android_versions_json
    @ios_devices_json = ios_devices_json
    @screen_resolutions_json = screen_resolutions_json
    @android_devices_json = android_devices_json
    # byebug
  end
end
