class CreateGeneralShares < ActiveRecord::Migration[5.0]
  def change
    create_table :general_shares do |t|
      t.string :source
      t.integer :year
      t.integer :month
      t.float :ios
      t.float :android

      t.timestamps
    end
  end
end
