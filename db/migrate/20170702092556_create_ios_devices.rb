class CreateIosDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :ios_devices do |t|
      t.string :source
      t.integer :year
      t.integer :month
      t.string :device
      t.float :percentage

      t.timestamps
    end
  end
end
