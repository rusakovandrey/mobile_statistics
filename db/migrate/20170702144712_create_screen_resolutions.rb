class CreateScreenResolutions < ActiveRecord::Migration[5.0]
  def change
    create_table :screen_resolutions do |t|
      t.string :source
      t.integer :year
      t.integer :month
      t.string :screen
      t.float :percentage

      t.timestamps
    end
  end
end
