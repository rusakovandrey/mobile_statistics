class CreateIosVersions < ActiveRecord::Migration[5.0]
  def change
    create_table :ios_versions do |t|
      t.string :source
      t.integer :year
      t.integer :month
      t.string :platform
      t.integer :version_maj
      t.integer :version_min
      t.float :percentage

      t.timestamps
    end
  end
end
