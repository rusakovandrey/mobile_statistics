class CreateAndroidVersions < ActiveRecord::Migration[5.0]
  def change
    create_table :android_versions do |t|
      t.string :source
      t.integer :year
      t.integer :month
      t.string :version
      t.float :percentage

      t.timestamps
    end
  end
end
