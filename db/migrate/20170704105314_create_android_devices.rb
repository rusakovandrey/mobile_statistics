class CreateAndroidDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :android_devices do |t|
      t.string :source
      t.integer :year
      t.integer :month
      t.string :device
      t.float :percentage

      t.timestamps
    end
  end
end
