require 'open-uri'
require 'openssl'
# Parsers of sites
module Parsers
  include Utils
  def unity3d_parser_ios
    page = Nokogiri::HTML(open('https://hwstats.unity3d.com/mobile/os-ios.html'))
    source = 'unity3d'
    platform = 'iOS'

    datetime = page.css('span.note').text
    year = datetime[0...datetime.index('-')].to_i
    month = datetime[datetime.index('-') + 1...datetime.length].to_i

    arr = page.css('.g3 ul li').text.split('%')
    arr.collect! { |x| x[x.index(' ') + 1, x.length] }
    delimeter = arr.count
    arr.each_with_index do |x, y|
      delimeter = y unless x.index('click').nil?
    end
    (arr.count - delimeter).times { arr.pop }
    arr.each do |x|
      version_maj = x[0, x.index('.')].to_i
      version_min = x[x.index('.') + 1, 1].to_i
      percentage = x[x.index(':') + 2, x.length - (x.index(':') + 2)].to_f
      vers = IosVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.platform = platform
      vers.version_maj = version_maj
      vers.version_min = version_min
      vers.percentage = percentage
      vers.save
    end
  end

  def david_smith_parser_ios
    page = Nokogiri::HTML(open('https://david-smith.org/iosversionstats/'))
    source = 'david-smith'
    platform = 'iOS'

    datetime = page.css('center')[2].text
    datetime = datetime[datetime.index(':') + 2, datetime.length - (datetime.index(':') + 2)]
    month_word = datetime[0, datetime.index(' ')]
    month = month_parser(month_word)
    year = datetime[datetime.index(',') + 2, 4].to_i

    arr = page.css('table')[0].text.split('%')
    arr.each do |x|
      version_maj = x[0, x.index('X') - 1].to_i
      percentage = x[x.index('X') + 1, x.length - (x.index('X') + 1)].to_f
      vers = IosVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.platform = platform
      vers.version_maj = version_maj
      vers.percentage = percentage
      vers.save
    end
  end

  def apple_parser_ios
    page = Nokogiri::HTML(open('https://developer.apple.com/support/app-store/'))
    source = 'apple'
    platform = 'iOS'

    date_string = page.css('p[class=footnote]')[0].text
    date_string = date_string[date_string.index('on') + 3, (date_string.length - (date_string.index('on') + 3))]
    month_string = date_string[0, date_string.index(' ')]
    month = month_parser(month_string)
    year = date_string[date_string.index(',') + 2, date_string.length - date_string.index(',') - 3].to_i

    jjs = Nokogiri::HTML(open('https://developer.apple.com/support/includes/ios-chart/scripts/chart.js')).text
    tmp_string = jjs[jjs.index('var newData '), jjs.index('var textRadius') - jjs.index('var newData')]
    until tmp_string.index('name').nil?
      tmp_string = tmp_string[tmp_string.index(':')...tmp_string.length]
      version_maj = tmp_string[tmp_string.index(':') + 6...tmp_string.index(',') - 1].to_i
      percentage = tmp_string[tmp_string.index('value') + 6...tmp_string.index('color') - 2].to_f
      tmp_string = tmp_string[tmp_string.index('}')...tmp_string.length]

      vers = IosVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.platform = platform
      vers.version_maj = version_maj
      vers.percentage = percentage
      vers.save unless vers.version_maj.zero?
    end
  end

  def apteligent_parser_ios
    page = Nokogiri::HTML(open('https://data.apteligent.com/ios/', ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE))
    source = 'apteligent'
    platform = 'iOS'
    date_string = page.css('span[class=chart-actions]').text
    year = date_string[date_string.index(':') + 3, 4].to_i
    month = date_string[date_string.index('-') + 1, 2].to_i

    data_arr = page.css('table').css('tr').text.gsub(/\s+/, ' ')
    data_arr = data_arr[data_arr.index('Distribution') + 13...data_arr.length]
    until data_arr.index('%').nil?
      version_maj = data_arr[0...data_arr.index('.')].to_i
      version_min = data_arr[data_arr.index('.') + 1...data_arr.index(' ')].to_i
      data_arr = data_arr[data_arr.index('iOS') + 4...data_arr.length]
      percentage = data_arr[data_arr.index(' ') + 1...data_arr.index('%')].to_f
      data_arr = data_arr[data_arr.index('%') + 2...data_arr.length]

      vers = IosVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.platform = platform
      vers.version_maj = version_maj
      vers.version_min = version_min
      vers.percentage = percentage
      vers.save unless vers.version_maj.zero?
    end
  end

  def bugfender_parser_ios
    source = 'bugfender'
    platform = 'iOS'
    month = Date.today.strftime('%m').to_i
    year = Date.today.strftime('%Y').to_i

    data = JSON.parse(open('https://app.bugfender.com/stats/ios-version-distribution').read)
    data['data'].each do |x|
      version_maj = x['name'].to_i
      percentage = x['y'].to_f

      vers = IosVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.platform = platform
      vers.version_maj = version_maj
      vers.percentage = percentage
      vers.save unless vers.version_maj.zero?
    end
  end

  def bugfender_parser_android_version
    source = 'bugfender'
    month = Date.today.strftime('%m').to_i
    year = Date.today.strftime('%Y').to_i
    data = JSON.parse(open('https://app.bugfender.com/stats/android-version-distribution').read)
    data['data'].each do |x|
      version = x['name']
      percentage = x['y'].to_f

      vers = AndroidVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.version = version
      vers.percentage = percentage
      vers.save unless vers.version.nil?
    end
  end

  def android_parser_android_version
    page = Nokogiri::HTML(open('https://developer.android.com/about/dashboards/index.html', ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE))
    source = 'android'
    date_string = page.css('em')[0].text
    date_string = date_string[date_string.index('ending on') + 10...date_string.length - date_string.index('.') - 3]
    month = month_parser(date_string[0...date_string.index(' ')])
    year =  date_string[date_string.index(',') + 2...date_string.length].to_i
    raw_script = page.at('/html/body').css('script').text
    raw_script = raw_script[raw_script.index('VERSION_DATA')...raw_script.index('VERSION_NAMES')]
    raw_script = raw_script[raw_script.index('data')...raw_script.length]

    until raw_script.index('api').nil?
      raw_script = raw_script[raw_script.index('api')...raw_script.length]
      api = raw_script[raw_script.index(':') + 2...raw_script.index(',')].to_i
      version = android_api_to_version(api)
      raw_script = raw_script[raw_script.index(',') + 1...raw_script.length]
      percentage = raw_script[raw_script.index('perc') + 8...raw_script.index('.') + 2].to_f
      raw_script = raw_script[raw_script.index('}') + 1...raw_script.length]

      vers = AndroidVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.version = version
      vers.percentage = percentage
      vers.save unless vers.version.nil?
    end
  end

  def apteligent_parser_android_version
    page = Nokogiri::HTML(open('https://data.apteligent.com/android/'))
    source = 'apteligent'
    date_string = page.css('span[class=chart-actions]').text
    date_string = date_string[date_string.index('  ') + 2...date_string.length]
    year = date_string[0...date_string.index('-')].to_i
    date_string = date_string[date_string.index('-') + 1...date_string.length]
    month = date_string[0...date_string.index('-')].to_i

    arr = page.css('td').to_a
    arr =  arr.map(&:text)
    arr.each_slice(4) do |x|
      version = x[0]
      version.delete!(' ') unless version.index(' ').nil?
      percentage = x[3].to_f

      vers = AndroidVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.version = version
      vers.percentage = percentage
      vers.save unless vers.version.nil?
    end
  end

  def appbrain_parser_android_version
    page = Nokogiri::HTML(open('http://www.appbrain.com/stats/top-android-sdk-versions'))
    source = 'appbrain'
    date_string = page.css('div[class="stats-segment"]')[0].text
    date_string = date_string[date_string.index('as of') + 6...date_string.index('.')]
    month = month_parser(date_string[0...date_string.index(' ')])
    year = date_string[date_string.index(',') + 2...date_string.length].to_i
    raw_script = page.css('script').text
    raw_script = raw_script[raw_script.index('data')...raw_script.index('chartData')]

    until raw_script.index('currentPercentage').nil?
      raw_script = raw_script[raw_script.index('currentPercentage')...raw_script.length]
      percentage = raw_script[raw_script.index(':') + 1...raw_script.index(',')]
      raw_script = raw_script[raw_script.index('key')...raw_script.length]
      version = raw_script[raw_script.index('key') + 6...raw_script.index(' ')]
      raw_script[raw_script.index(')')...raw_script.length]

      vers = AndroidVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.version = version
      vers.percentage = percentage
      vers.save unless vers.version.nil?
    end
  end

  def unity3d_parser_android_version
    page = Nokogiri::HTML(open('https://hwstats.unity3d.com/mobile/os-android.html'))
    source = 'unity3d'

    datetime = page.css('span.note').text
    year = datetime[0...datetime.index('-')].to_i
    month = datetime[datetime.index('-') + 1...datetime.length].to_i

    arr = page.css('.g3 ul li').text.split('%')
    arr.collect! { |x| x[x.index(' ') + 1, x.length] }
    delimeter = arr.count
    arr.each_with_index do |x, y|
      delimeter = y unless x.index('click').nil?
    end
    (arr.count - delimeter).times { arr.pop }
    arr.each do |x|
      version = x[0, x.index(':')]
      percentage = x[x.index(':') + 2, x.length - (x.index(':') + 2)].to_f

      vers = AndroidVersion.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.version = version
      vers.percentage = percentage
      vers.save unless vers.version.nil?
    end
  end

  def netmarketshare_parser_general_share
    page = Nokogiri::HTML(open('https://www.netmarketshare.com/operating-system-market-share.aspx?qprid=8&qpcustomd=1'))
    source = 'netmarketshare'
    date_string = page.css('div')[28].css('div').css('td')[1].text
    month = month_parser(date_string[0...date_string.index(',')])
    year = date_string[date_string.index(' ') + 1...date_string.length].to_i

    tmp_string = page.css('table[id=fwReportTable1]').text
    tmp_string = tmp_string[tmp_string.index('Android')...tmp_string.length]
    arr = tmp_string.split('%')
    ios = 0.0
    android = 0.0

    arr.each do |x|
      num = x[/\d+/]
      platform = x[0...x.index(num)]
      platform[0] = '' if platform[0] == ' '
      percentage =  x[x.index(num)...x.length].to_f
      ios = percentage if platform == 'iOS'
      android = percentage if platform == 'Android'
    end

    vers = GeneralShare.new
    vers.source = source
    vers.year = year
    vers.month = month
    vers.ios = ios
    vers.android = android
    vers.save if vers.ios != 0.0 && vers.android != 0.0
  end

  def statcounter_parser_general_share
    page = Nokogiri::HTML(open('http://gs.statcounter.com/os-market-share/mobile/worldwide'))
    source = 'statcounter'
    date_string = page.css('th')[1].text
    month = month_parser(date_string[0...date_string.index(' ')])
    year = date_string[date_string.index(' ') + 1...date_string.length].to_i

    raw_script = page.css('table[class=stats-snapshot]').text
    raw_script = raw_script[raw_script.index('Android')...raw_script.length]
    arr = raw_script.split('%')
    ios = 0.0
    android = 0.0
    arr.each do |x|
      x.delete!("\n")
      x.delete!("\t")
      x.delete!(' ')
      num = x[/\d+/]
      platform = x[0...x.index(num)] unless num.nil?

      percentage = x[x.index(num)...x.length].to_f unless num.nil?
      ios = percentage if platform == 'iOS'
      android = percentage if platform == 'Android'
    end
    vers = GeneralShare.new
    vers.source = source
    vers.year = year
    vers.month = month
    vers.ios = ios
    vers.android = android
    vers.save if vers.ios != 0.0 && vers.android != 0.0
  end

  def unity3d_parser_general_share
    page = Nokogiri::HTML(open('http://hwstats.unity3d.com/mobile/os.html'))
    source = 'unity3d'
    datetime = page.css('span.note').text
    year = datetime[0...datetime.index('-')].to_i
    month = datetime[datetime.index('-') + 1...datetime.length].to_i

    ios = 0.0
    android = 0.0
    arr = page.css('.g3 ul li').text.split('%')
    arr.each do |x|
      break unless x.index('Others').nil?
      platform = x[0...x.index(':')]
      percentage = x[x.index(' ') + 1...x.length].to_f
      ios = percentage if platform == 'iOS'
      android = percentage if platform == 'Android'
    end
    vers = GeneralShare.new
    vers.source = source
    vers.year = year
    vers.month = month
    vers.ios = ios
    vers.android = android
    vers.save if vers.ios != 0.0 && vers.android != 0.0
  end

  def w3schools_parser_general_share
    page = Nokogiri::HTML(open('https://www.w3schools.com/browsers/browsers_mobile.asp'))
    source = 'w3schools'

    arr1 = page.css('tr')[0].children.map(&:text).uniq
    arr1.shift
    arr2 = page.css('tr')[1].children.map(&:text).uniq
    arr2.shift

    year = arr1[0].to_i
    month = month_parser(arr2[0])

    ios = 0.0
    android = 0.0
    total = 0.0
    (1...arr1.length).each do |i|
      android = arr2[i].to_f if arr1[i] == 'Android'
      ios = arr2[i].to_f if arr1[i] == 'iOS*'
      total = arr2[i].to_f if arr1[i] == 'Total'
    end

    ios = (ios / total * 100).round(2)
    android = (android / total * 100).round(2)
    vers = GeneralShare.new
    vers.source = source
    vers.year = year
    vers.month = month
    vers.ios = ios
    vers.android = android
    vers.save if vers.ios != 0.0 && vers.android != 0.0
  end

  def bugfender_parser_ios_devices
    source = 'bugfender'
    month = Date.today.strftime('%m').to_i
    year = Date.today.strftime('%Y').to_i

    data = JSON.parse(open('https://app.bugfender.com/stats/ios-device-distribution').read)
    data['data'].each do |x|
      device = x['name']
      percentage = x['y'].to_f

      vers = IosDevice.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.device = device
      vers.percentage = percentage
      vers.save unless vers.percentage.zero?
    end
  end

  def david_smith_parser_ios_devices
    page = Nokogiri::HTML(open('https://david-smith.org/iosversionstats/'))
    source = 'david-smith'

    datetime = page.css('center')[2].text
    datetime = datetime[datetime.index(':') + 2, datetime.length - (datetime.index(':') + 2)]
    month_word = datetime[0, datetime.index(' ')]
    month = month_parser(month_word)
    year = datetime[datetime.index(',') + 2, 4].to_i

    page.css('table')[3].children.css('tr')[0].children.to_a
    page.css('table')[3].children.css('tr').count # 35
    page.css('table')[3].children.css('tr')[33].children.to_a # 33 (-overall)

    (2...page.css('table')[3].children.css('tr').count - 1).each do |x|
      y = page.css('table')[3].children.css('tr')[x].children.to_a
      device = y[0].text
      percentage = y[1].text.to_f

      vers = IosDevice.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.device = device
      vers.percentage = percentage
      vers.save unless vers.percentage.zero?
    end
  end

  def unity3d_parser_ios_devices
    page = Nokogiri::HTML(open('https://hwstats.unity3d.com/mobile/device-ios.html'))
    source = 'unity3d'

    datetime = page.css('span.note').text
    year = datetime[0...datetime.index('-')].to_i
    month = datetime[datetime.index('-') + 1...datetime.length].to_i

    arr = page.css('.g3')[1].css('ul li').text.split('%')
    arr.each do |x|
      x.delete("\n") unless x.index("\n").nil?
      break unless x.index('Others').nil?
      device = x[0...x.index(':')]
      percentage = x[x.index(' ') + 1...x.length].to_f

      vers = IosDevice.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.device = device
      vers.percentage = percentage
      vers.save if vers.percentage != 0
    end
  end

  def statcounter_parser_screen_resolution
    page = Nokogiri::HTML(open('http://gs.statcounter.com/screen-resolution-stats/mobile/united-states-of-america'))
    source = 'statcounter'
    date_string = page.css('th')[1].text
    month = month_parser(date_string[0...date_string.index(' ')])
    year = date_string[date_string.index(' ') + 1...date_string.length].to_i

    raw_script = page.css('table[class=stats-snapshot]').text
    raw_script = raw_script[raw_script.index(year.to_s) + 4...raw_script.length]
    raw_script = raw_script.delete("\n\t").gsub(/\s+/, ' ')

    arr = raw_script.split('%')
    arr.each do |x|
      screen = 'error'
      percentage = 0
      x[0] = ''
      screen = x[0...x.index(' ')] unless x.index(' ').nil?
      percentage = x[x.index(' ') + 1...x.length].to_f unless x.index(' ').nil?

      vers = ScreenResolution.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.screen = screen
      vers.percentage = percentage
      vers.save if percentage != 0
    end
  end

  def unity3d_parser_screen_resolution
    page = Nokogiri::HTML(open('https://hwstats.unity3d.com/mobile/display.html'))
    source = 'unity3d'

    datetime = page.css('span.note').text
    year = datetime[0...datetime.index('-')].to_i
    month = datetime[datetime.index('-') + 1...datetime.length].to_i
    arr = page.css('.g3')[0].css('ul li').text.split('%')
    arr.each do |x|
      x.delete("\n") unless x.index("\n").nil?
      break unless x.index('Others').nil?
      screen = x[0...x.index(':')]
      screen.delete!(' ')
      percentage = x[x.index(':') + 1...x.length].to_f

      vers = ScreenResolution.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.screen = screen
      vers.percentage = percentage
      vers.save if vers.percentage != 0
    end
  end

  def appbrain_parser_android_devices
    page = Nokogiri::HTML(open('https://www.appbrain.com/stats/top-devices-by-country?country=US'))
    source = 'appbrain'

    date_string = page.css('div[class="stats-pagetop-updated"]')[0].text.delete("\n")
    date_string = date_string[date_string.index(':') + 2...date_string.length]
    month = month_parser(date_string[0...date_string.index(' ')])
    year = date_string[date_string.index(',') + 2...date_string.length].to_i

    raw_script = page.css('script').text
    raw_script = raw_script[raw_script.index("\ndata")...raw_script.index("\nstats")]
    arr = raw_script.split('modelName')
    arr.shift
    arr.each do |x|
      x = x.delete(':')
      x = x.delete('"')
      device = x[0...x.index(',')]
      percentage = x[x.index('popularity') + 10...x.index('}')].to_f * 100

      vers = AndroidDevice.new
      vers.source = source
      vers.year = year
      vers.month = month
      vers.device = device
      vers.percentage = percentage
      vers.save if vers.percentage != 0
    end
  end
end
