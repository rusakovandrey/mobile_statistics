# Stuff to help other stuff
module Utils
  def parser_csv(what, filename)
    what.delete_all
    CSV.foreach(filename, headers: true) do |row|
      # byebug
      what.create!(row.to_hash) unless row.nil?
      # if !row.nil?
      # row != nil
    end
  end

  def month_parser(month_word)
    month =
      case month_word
      when 'January', 'Jan' then 1
      when 'February', 'Feb' then 2
      when 'March', 'Mar' then 3
      when 'April', 'Apr' then 4
      when 'May' then 5
      when 'June', 'Jun' then 6
      when 'July', 'Jul' then 7
      when 'August', 'Aug' then 8
      when 'September', 'Sep' then 9
      when 'October', 'Oct' then 10
      when 'November', 'Nov' then 11
      when 'December', 'Dec' then 12
      end
    month
  end

  def android_api_to_version(api)
    version =
      case api
      when 10 then '2.3.3-2.3.7'
      when 15 then '4.0.3-4.0.4'
      when 16 then '4.1.x'
      when 17 then '4.2.x'
      when 18 then '4.3'
      when 19 then '4.4'
      when 21 then '5.0'
      when 22 then '5.1'
      when 23 then '6.0'
      when 24 then '7.0'
      when 25 then '7.1'
      else 'ERROR'
      end
    version
  end

  def parse_android_version(str)
    version =
      case str
      when /^2\.0.*/, /^2\.1.*/ then '2.0-2.1'
      when /^2\.2.*/ then '2.2.x'
      when /^2\.3.*/ then '2.3.x'
      when /^3\.0.*/, /^3\.1.*/, /^3\.2.*/ then '3.0-3.2'
      when /^4\.0.*/ then '4.0.1-4.0.4'
      when /^4\.1.*/, /^4\.2.*/, /^4\.3.*/ then '4.1-4.3'
      when /^4\.4.*/ then '4.4-4.4.4'
      when /^5\.0.*/, /^5\.1.*/ then '5.0-5.1'
      when /^6\.0.*/ then '6.0'
      when /^7\.0.*/, /^7\.1.*/ then '7.0-7.1'
      when /^8\.0.*/ then '8.0.x'
      else 'error'
      end
    version
  end
end
