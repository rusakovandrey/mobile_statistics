# Processing data, constructing jsons
module Jsons
  include Utils
  def major_ios_versions_average_json
    ios_versions = IosVersion.all

    # get all major versions list and num of sources
    ios_major_versions = {}
    ios_sources = {}
    IosVersion.all.each do |v|
      ios_major_versions[v.version_maj] = 0
      ios_sources[v.source] = 0
    end

    # get averages
    ios_versions.each do |v|
      ios_major_versions[v.version_maj] += v.percentage
    end
    ios_major_versions.each_pair do |key, value|
      ios_major_versions[key] = value / ios_sources.length
    end
    total = 0
    ios_major_versions.each_value do |value|
      total += value
    end
    coeff = 100 / total
    ios_major_versions_corrected = {}
    ios_major_versions.each_pair do |key, value|
      ios_major_versions_corrected[key] = (value * coeff).round(2)
    end
    ios_versions_list = ios_versions.map do |g|
      { source: g.source, year: g.year, month: g.month, version_maj: g.version_maj, version_min: g.version_min, percentage: g.percentage }
    end
    # json = { for_table: ios_versions_list, for_diagram: ios_major_versions_corrected }.to_json
    for_diagram = []
    ios_major_versions_corrected.each_pair do |key,value|
      for_diagram <<  { label: key, value: value }
    end

    json = { for_table: ios_versions_list, for_diagram: for_diagram }.to_json
    json
  end

  def general_shares_json
    general_shares = GeneralShare.all
    ios_average = GeneralShare.pluck('ios').reduce(:+) / GeneralShare.pluck('ios').size.to_f
    android_average = GeneralShare.pluck('android').reduce(:+) / GeneralShare.pluck('android').size.to_f
    coeff = 100 / (ios_average + android_average)
    ios_average_corrected = (ios_average * coeff).round(2)
    android_average_corrected = (android_average * coeff).round(2)

    general_shares_list = general_shares.map do |g|
      { source: g.source, year: g.year, month: g.month, ios: g.ios, android: g.android }
    end
    ios = { label: 'iOS', value: ios_average_corrected }
    android = { label: 'Android', value: android_average_corrected }
    json = { for_table: general_shares_list, for_diagram: [ios,android] }.to_json
    json
  end

  def android_versions_json
    android_versions = AndroidVersion.all
    # get list of sources
    sources_list = AndroidVersion.pluck('source').uniq

    versions_with_percentage = Hash.new(0)
    sources_list.each do |x|
      tmp_hash = Hash.new(0)
      android_versions.each do |y|
        # generalize versions
        tmp_hash[parse_android_version(y.version)] += y.percentage if y.source == x
      end
      # get scale to get 100%
      total = 0.0
      tmp_hash.each_value do |value|
        total += value
      end
      coeff = 100 / total
      tmp_hash.each_key do |key|
        # rewrite with coefficient
        versions_with_percentage[key] += tmp_hash[key] * coeff unless key == 'error'
      end
    end

    versions_with_percentage.each_key do |key|
      # division by n, where n == number of sources
      versions_with_percentage[key] = (versions_with_percentage[key] / sources_list.length).round(3)
    end

    android_versions_list = android_versions.map do |g|
      { source: g.source, year: g.year, month: g.month, version: g.version, percentage: g.percentage }
    end
    for_diagram = []
    versions_with_percentage.each_pair do |key,value|
      for_diagram <<  { label: key, value: value }
    end

    json = { for_table: android_versions_list, for_diagram: for_diagram }.to_json
    json
  end

  def ios_devices_json
    ios_devices = IosDevice.all
    ios_devices_corrected = Hash.new(0)
    total2 = 0
    # get list of sources
    sources_list = IosDevice.pluck('source').uniq
    sources_list.each do |x|
      tmp_hash = Hash.new(0)
      ios_devices.each do |y|
        tmp_hash[y.device] += y.percentage if y.source == x
      end
      total = 0
      tmp_hash.each_value do |value|
        total += value
      end
      coeff = 100 / total

      tmp_hash.each_pair do |key, value|
        ios_devices_corrected[key] += value * coeff
        total2 += value * coeff
      end
    end
    ios_devices_corrected.each_pair do |key, value|
      ios_devices_corrected[key] = (value / sources_list.length).round(3)
    end

    ios_devices_list = ios_devices.map do |g|
      { source: g.source, year: g.year, month: g.month, device: g.device, percentage: g.percentage }
    end

    json = { for_table: ios_devices_list, for_diagram: ios_devices_corrected }.to_json
    json
  end

  def screen_resolutions_json
    screen_resolutions = ScreenResolution.all
    json = { for_table: screen_resolutions, for_diagram: screen_resolutions }.to_json
    json
  end

  def android_devices_json
    android_devices = AndroidDevice.all
    sources_list = AndroidDevice.pluck('source').uniq
    tmp_hash = Hash.new(0)
    android_devices.each do |x|
      x.device.delete!(' ')
      tmp_hash[x.device] += x.percentage
    end
    total = 0
    tmp_hash.each_value do |value|
      total += value
    end
    coeff = 100 * sources_list.length / total
    total2 = 0
    tmp_hash.each_pair do |key, value|
      tmp_hash[key] = (value * coeff / sources_list.length).round(3)
      total2 += tmp_hash[key]
    end

    android_devices_list = android_devices.map do |g|
      { source: g.source, year: g.year, month: g.month, device: g.device, percentage: g.percentage }
    end

    json = { for_table: android_devices_list, for_diagram: tmp_hash }.to_json
    json
  end
end
