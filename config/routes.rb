Rails.application.routes.draw do
  get 'parser/index'
  # resources :parsers
  post 'parser/init', controller: 'ParserController#init'
  get 'parser/init', to: redirect('parser/index')

  root 'parser#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
